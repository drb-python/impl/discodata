.. _api:

Reference API
=============

DrbDiscodataServiceNode
-----------------------

Represents the DISCODATA service. This node has no attribute and has as children
the database of the service (DrbDiscodataDataBaseNode)

.. autoclass:: drb.drivers.discodata.discodata.DrbDiscodataServiceNode
    :members:

DrbDiscodataDataBaseNode
------------------------

Represents the Database contains in the DISCODATA service. Those nodes have no attribute and have as children
the table (DrbDiscodataTableNode)

.. autoclass:: drb.drivers.discodata.discodata.DrbDiscodataDataBaseNode
    :members:


DrbDiscodataTableNode
---------------------

Represents the Table contains in the differents database of the service

We can access to each row of the Table by index or slice
.. code-block:: python

    table = tb['AirQualityStatistics']

    rows_10_to_20 = table [10:20]

.. autoclass:: drb.drivers.discodata.discodata.DrbDiscodataTableNode
    :members: