from drb.drivers.discodata.discodata import DrbDiscodataServiceNode

node = DrbDiscodataServiceNode()

print(node.attribute_names())

for db in node:

    print("DB :" + db.name)
    for tab in db:
        print("     TB : " + tab.name)
#
tb = node['AirQualityDataFlows']
print(tb.attribute_names())

table = tb['AirQualityStatistics']

print(table.name)
print(table.attribute_names())

#
print(len(table))

fist_col = True
for i in range(1038, 1041):
    row = table[i]
    if fist_col:
        line = ''
        line = row[0].name
        for col in row[1:]:
            line += '|' + col.name
        print(line)
        fist_col = False
    line = row[0].value
    for col in row[1:]:
        line += '|' + str(col.value)
    print(line)
row = table[4090092]
line = row[0].value
for col in row[1:]:
    line += '|' + str(col.value)
print(line)
