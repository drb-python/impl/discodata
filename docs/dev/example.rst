.. _example:

Examples
=========

Open and read into AirQualityDataFlows database the AirQualityStatistics table
-----------------------------
.. literalinclude:: example/read.py
    :language: python
