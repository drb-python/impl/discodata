===================
Data Request Broker
===================
---------------------------------
DISCODATA driver for DRB
--------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-discodata/month
    :target: https://pepy.tech/project/drb-driver-discodata
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-discodata.svg
    :target: https://pypi.org/project/drb-driver-discodata/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-discodata.svg
    :target: https://pypi.org/project/drb-driver-discodata/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-discodata.svg
    :target: https://pypi.org/project/drb-driver-discodata/
    :alt: Python Version Support Badge

-------------------

This module implements grib format access with DRB data model.
It is able to navigates among the discodata contents.

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

